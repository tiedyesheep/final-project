import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;

public class Sprite {
	private Body body;
	private Stats stats;
	Weapon weapon;
	private int direction = 1;
	private Point2D.Double pos;
	private double yv = 0,xv=0;
	public Controller controller;
	private Animation state;
	boolean iog = false;
	public double health;
	public Sprite(Stats stats, Body body, Weapon weapon, Controller controller, Point2D.Double startPos){
		this.stats = stats;
		this.body = body;
		this.weapon = weapon;
		this.pos = startPos;
		controller.setParent(this);
		this.state = body.getStationaryAnimation();
		this.controller = controller;
		health = stats.getHealth();
	}
	public Animation getState(){
		return state;
	}
	public void setState(Animation ani){
		this.state = ani;
	}
	public void tick(){
		weapon.tick();
	}
	public void setJump(boolean tof){
		iog = true;
		if(true){
			if(direction>0){
				setState(body.getRightAnimation());
			}else if(direction<0){
				setState(body.getLeftAnimation());
			}else{
				setState(body.getStationaryAnimation());
			}
		}
	}
	public Stats getStats(){
		return stats;
	}
	public Body getBody(){
		return body;
	}
	public Weapon getWeapon(){
		return weapon;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int i) {
		direction = i;
	}
	public void forward(int i) {
		xv=i;
	}
	public boolean isOnGround() {
		//TODO stuff
		return iog;
	}
	public void jump(int jump) {
		yv+=jump;
		iog = false;
	}
	public void useWeapon(Model m) {
		weapon.use(this, m);
	}
	public void translatePos(double x, double y){
		pos = new Point2D.Double(pos.getX()+x, pos.getY()+y);
		
	}
	public Point2D.Double getPos(){
		return pos;
	}
	public Point2D.Double getVelocity(){
		return new Point2D.Double(xv, yv);
	}
	public void setVelocity(double x, double y){
		xv = x;
		yv = y;
	}
	public void setPos(double x, double y){
		this.pos = new Point2D.Double(x,y);
	}
}
