import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

public class BasicPlayer extends Player {
	public BasicPlayer(Stats stats, Body body, Weapon weapon, PlayerController controller, Double startPos) {
		super(stats, body, weapon, controller, startPos);
	}

	public static BasicPlayer BPFactory(Point2D.Double startPos){
		Stats stats = new Stats(10,20,10,0,300);
		Body body = new Body();
		body.setStationaryAnimation(new Animation(Utils.getImage("res/DarthVader.png"),new Point(88,163-134)));
		body.setLeftAnimation(new Animation(Utils.getImage("res/DarthVader.png"),new Point(28,163-134)));
		body.setRightAnimation(new Animation(Utils.getImage("res/DarthVader.png"),new Point(88,163-134)));
		body.setJumpingAnimation(new Animation(Utils.getImage("res/DarthVader.png"),new Point(88,163-134)));
		body.setStrikingAnimation(new Animation(Utils.getImage("res/DarthVader.png"),new Point(88,163-134)));
		Weapon weapon = new LightSaber();
		PlayerController pc = new PlayerController();
		return new BasicPlayer(stats,body,weapon,pc, startPos);
	}
}