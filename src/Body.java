

public class Body {
	private Animation[] animations;
	public Body() {
		this.animations = new Animation[5];
	}
	public Animation getStationaryAnimation(){
		return animations[0];
	}
	public Animation getRightAnimation(){
		return animations[1];
	}
	public Animation getLeftAnimation(){
		return animations[2];
	}
	public Animation getStrikingAnimation(){
		return animations[3];
	}
	public Animation getJumpingAnimation(){
		return animations[4];
	}
	public void setStationaryAnimation(Animation a){
		animations[0]=a;
	}
	public void setRightAnimation(Animation a){
		animations[1]=a;
	}
	public void setLeftAnimation(Animation a){
		animations[2]=a;
	}
	public void setStrikingAnimation(Animation a){
		animations[3]=a;
	}
	public void setJumpingAnimation(Animation a){
		animations[4]=a;
	}
}
