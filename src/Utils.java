import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class Utils {
	public static boolean collide(Polygon2 p1, Polygon2 p2){
		Rectangle r1 = p1.getBounds();
		
		
		Rectangle r2 = p2.getBounds();
		r1.setLocation((int)p1.pos.x,(int)p1.pos.y);
		r2.setLocation((int)p2.pos.x,(int)p2.pos.y);
		//System.out.println("Rect1: "+r1.x+", "+r1.y+", "+(r1.width+r1.x)+", "+(r1.height+r1.y)+" Rect2:"+r2.x+", "+r2.y+", "+(r2.width+r2.x)+", "+(r2.height+r2.y));
		if(r1.intersects(r2)){
			if(p1.npoints()>p2.npoints()){
				Polygon2 temp = p1;
				p1 = p2;
				p2 = temp;
			}
			for(int i = 0;i<p1.npoints();i++){
				if(p2.contains(p1.xpoints()[i]+(int)p1.getPos().x,p1.ypoints()[i]+(int)p1.getPos().y)){
					return true;
				}
			}
			for(int i = 0;i<p2.npoints();i++){
				if(p1.contains(p2.xpoints()[i]+(int)p2.getPos().x,p2.ypoints()[i]+(int)p2.getPos().y)){
					return true;
				}
			}
		}
		return false;
	}
	public static Image getImage(String path){
		return new ImageIcon(path).getImage();
	}
	public static int cdirection(Polygon2 p1, Polygon2 p2){
		Rectangle r1 = p1.getBounds();
		r1.setLocation((int)p1.pos.x,(int)p1.pos.y);
		Rectangle r2 = p2.getBounds();
		r2.setLocation((int)p2.pos.x,(int)p2.pos.y);
		//Point t = new Point(r1.x+r1.width/2,r1.y);
		//Point r = new Point(r1.x+r1.width,r1.y+r1.height/2);
		//Point b = new Point(r1.x+r1.width/2,r1.y+r1.height);
		//Point l = new Point(r1.x,r1.y+r1.height/2);
		//Point c = new Point((int)r2.getCenterX(),(int)r2.getCenterY());
		int r = Math.abs(r1.x+r1.width-r2.x);
		int b = Math.abs(r1.y+r1.height-r2.y);
		int l = Math.abs(r1.x-(r2.x+r2.width));
		int t = Math.abs(r1.y-(r2.y+r2.height));
		double dist = 500;
		int side = 0;
		if(collide(p1,p2)){
			if(t<dist){
				side = 1;
				dist = t;
			}
			if(r<dist){
				side = 2;
				dist = r;
			}
			if(b<dist){
				side = 3;
				dist = b;
			}
			if(l<dist){
				side = 4;
				dist = l;
			}
			//System.out.println(dist);
			return side;
		}else{
			return 0;
		}
	}
	public static Polygon getPolygon(Image img){
		BufferedImage i = toBufferedImage(img);
		Point[] points = MakePoly(i,4,10);
		int[] xpoints = new int[points.length];
		int[] ypoints = new int[points.length];
		for(int a = 0; a<points.length;a++){
			xpoints[a] = points[a].x;
			ypoints[a] = points[a].y;
		}
		Polygon p = new Polygon(xpoints,ypoints, points.length);
		return p;
	}
	public static BufferedImage toBufferedImage(Image img)
	{
	    if (img instanceof BufferedImage)
	    {
	        return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
    private static Point[] MakePoly(BufferedImage spr,int d,int angle){

    	//creates an outline of a transparent image, points are stored in an array
    	//arg0 - BufferedImage source image 
    	//arg1 - Int detail (lower = better)
    	//arg2 - Int angle threshold in degrees (will remove points with angle differences below this level; 15 is a good value)
    	//      making this larger will make the body faster but less accurate;
    	int w= spr.getWidth(null);
    	int h= spr.getHeight(null);
    	// increase array size from 255 if needed
    	int size = 16000;
    	int[] vertex_x=new int[size], vertex_y=new int[size], vertex_k=new int[size]; 
    	int numPoints=0, tx=0,ty=0,fy=-1,lx=0,ly=0;
    	vertex_x[0]=0;
    	vertex_y[0]=0;
    	vertex_k[0]=1; 
    	for (tx=0;tx<w;tx+=d){
    		for (ty=0;ty<h;ty+=1){
    			if((spr.getRGB(tx,ty)>>24) != 0x00 ) {
    				vertex_x[numPoints]=tx;
    				vertex_y[numPoints]=h-ty;
    				vertex_k[numPoints]=1;
    				numPoints++;
    				if (fy<0)
    					fy=ty;
    				lx=tx;
    				ly=ty;
    				break;
    			}      
    		}
    	}
    	for (ty=0;ty<h;ty+=d){
    		for (tx=w-1;tx>=0;tx-=1){
    			if((spr.getRGB(tx,ty)>>24)!= 0x00 && ty > ly){
    				vertex_x[numPoints]=tx;
    				vertex_y[numPoints]=h-ty;
    				vertex_k[numPoints]=1;
    				numPoints++;
    				lx=tx;
    				ly=ty;
    				break;
    			} 
    		}
    	}
    	for (tx=w-1;tx>=0;tx-=d){
    		for (ty=h-1;ty>=0;ty-=1){
    			if((spr.getRGB(tx,ty)>>24) != 0x00 && tx < lx){
    				vertex_x[numPoints]=tx;
    				vertex_y[numPoints]=h-ty;
    				vertex_k[numPoints]=1;
    				numPoints ++;
    				lx=tx;
    				ly=ty;
    				break;
    			}     
    		}
    	}
    	for (ty=h-1;ty>=0;ty-=d){
    		for (tx=0;tx<w;tx+=1){
    			if((spr.getRGB(tx,ty)>>24) != 0x00 && ty < ly && ty > fy){
    				vertex_x[numPoints]=tx;
    				vertex_y[numPoints]=h-ty;
    				vertex_k[numPoints]=1;
    				numPoints ++;
    				lx=tx;
    				ly=ty;
    				break;
    			}      
    		}
    	}
    	double ang1,ang2;
    	for (int i=0;i<numPoints-2;i++) {
        	ang1 = PointDirection(vertex_x[i],vertex_y[i], vertex_x[i+1],vertex_y[i+1]);
        	ang2 = PointDirection(vertex_x[i+1],vertex_y[i+1], vertex_x[i+2],vertex_y[i+2]);
        	if (Math.abs(ang1-ang2) <= angle){
        		vertex_k[i+1] = 0;
        	}
        }
    	ang1 = PointDirection(vertex_x[numPoints-2],vertex_y[numPoints-2], vertex_x[numPoints-1],vertex_y[numPoints-1]);
    	ang2 = PointDirection(vertex_x[numPoints-1],vertex_y[numPoints-1], vertex_x[0],vertex_y[0]);
     	if (Math.abs(ang1-ang2) <= angle){
     		vertex_k[numPoints-1] = 0; 
     	}
     	ang1 = PointDirection(vertex_x[numPoints-1],vertex_y[numPoints-1], vertex_x[0],vertex_y[0]);
    	ang2 = PointDirection(vertex_x[0],vertex_y[0], vertex_x[1],vertex_y[1]);
     	if (Math.abs(ang1-ang2) <= angle){
     		vertex_k[0] = 0;
     	}
     	int n=0;
     	for (int i=0;i<numPoints;i++){
     		if(vertex_k[i]==1){
     			n++;
     		}
     	}
     	Point[] poly= new Point[n];
     	n=0;
     	for (int i=0;i<numPoints;i++){
     		if (vertex_k[i]==1){
     			poly[n]=new Point();
     			poly[n].x=vertex_x[i];
     			poly[n].y=h-vertex_y[i];
     			n++;
     		}
     	}
     	return poly;
    }

    private static double PointDirection(double xfrom,double yfrom,double xto,double yto){
    	return  Math.atan2(yto-yfrom,xto-xfrom)*180/Math.PI ;
	}
    public static void applyGrayscaleMaskToAlpha(BufferedImage image, BufferedImage mask)
    {
        int width = image.getWidth();
        int height = image.getHeight();

        int[] imagePixels = image.getRGB(0, 0, width, height, null, 0, width);
        int[] maskPixels = mask.getRGB(0, 0, width, height, null, 0, width);

        for (int i = 0; i < imagePixels.length; i++)
        {
            int color = imagePixels[i] & 0x00ffffff; // Mask preexisting alpha
            int alpha = maskPixels[i] << 24; // Shift blue to alpha
            imagePixels[i] = color | alpha;
        }

        image.setRGB(0, 0, width, height, imagePixels, 0, width);
    }

}
