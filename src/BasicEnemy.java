import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

public class BasicEnemy extends Enemy {
	public BasicEnemy(Stats stats, Body body, Weapon weapon, Ai ai,Point2D.Double startPos) {
		super(stats, body, weapon, ai, startPos);
	}
	public static BasicEnemy createInstance(Point2D.Double startPos) {
		Stats stats = new Stats(1,5,5,100,50);
		Animation[] anis = new Animation[5];
		Body body = new Body();
		body.setStationaryAnimation(new Animation(Utils.getImage("res/stormTrooper.png"),new Point(28,163-134)));
		body.setLeftAnimation(new Animation(Utils.getImage("res/stormTrooper.png"),new Point(28,163-134)));
		body.setRightAnimation(new Animation(Utils.getImage("res/stormTrooper.png"),new Point(88,163-134)));
		body.setJumpingAnimation(new Animation(Utils.getImage("res/stormTrooper.png"),new Point(28,163-134)));
		body.setStrikingAnimation(new Animation(Utils.getImage("res/stormTrooper.png"),new Point(88,163-134)));
		Weapon weapon = new Blaster(false);
		Ai ai = new Ai();
		return new BasicEnemy(stats,body,weapon,ai,startPos);
	}
}
