import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;


public class Terrain {
	public Polygon2 boundingBox;
	BufferedImage image;
	Point2D.Double pos;
	public Terrain(Polygon2 poly, BufferedImage img, Point2D.Double pos) {
		Rectangle r = poly.getBounds();
		BufferedImage mask = new BufferedImage(r.width, r.height, BufferedImage.TYPE_INT_ARGB);
		BufferedImage img2 = new BufferedImage(r.width, r.height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = mask.createGraphics();
		Graphics2D g2 = img2.createGraphics();
		for(int i = 0; i<r.width;i+=img.getWidth()){
			for(int a = 0; a<r.height;a+=img.getHeight()){
				g2.drawImage(img, null,i, a);
			}
		}
		g2.drawImage(img, null, 0, 0);
		g.setColor(new Color(255,255,255));
		g.fillPolygon(poly.poly);
		Utils.applyGrayscaleMaskToAlpha(img2, mask);
		image = img2;
		boundingBox = poly;
		this.pos = pos;
	}
	public Terrain(Polygon2 poly, Color c, Point2D.Double pos){
		Rectangle r = poly.getBounds();
		BufferedImage img = new BufferedImage(r.width, r.height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		g.setColor(c);
		g.fillPolygon(poly.poly);
		image = img;
		boundingBox = poly;
		this.pos = pos;
	}

}
