import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;


public class Model {
	boolean playing = true;
	ArrayList<Terrain> terrain;
	ArrayList<Enemy> enemies;
	CopyOnWriteArrayList<Bullet> ebullets;
	CopyOnWriteArrayList<Bullet> pbullets;
	BufferedImage image;
	Level level;
	Player player;
	boolean lose = false;
	boolean win = false;
	public Model(Level level, Player player) {
		this.level = level;
		this.player = player;
		terrain = level.getTerrain();
		enemies = level.getEnemies();
		ebullets = new CopyOnWriteArrayList<Bullet>();
		pbullets = new CopyOnWriteArrayList<Bullet>();
		Point spawn = level.getSpawn();
		this.player.setPos(spawn.x, spawn.y);
		((PlayerController)player.controller).setModel(this);
		//image = new BufferedImage(level.getBounds().width,level.getBounds().height,BufferedImage.TYPE_INT_ARGB);
	}
	public void update(Rectangle camera, Point camPos){
		image = new BufferedImage(camera.width,camera.height,BufferedImage.TYPE_INT_ARGB);
		Rectangle draw = new Rectangle((int)player.getPos().x-250,(int)level.getBounds().getHeight()-(int)player.getPos().y-250,500,500);
		Graphics2D g = image.createGraphics();
		g.fillRect(0, 0, image.getWidth(), (int)level.getBounds().getHeight());
		player.setVelocity(player.getVelocity().x, player.getVelocity().y-0.2);
		player.tick();
		for(Enemy e: enemies){
			if(e.weapon.dangerous){
				//they're not
			}
		}
		if(player.weapon.dangerous){
			Polygon ws;
			int x1;
			int y1;
			if(player.weapon.isUsed()){
				y1 = (int) (0)+(int)(player.getPos().y+player.getState().getHandPos().y-player.getWeapon().getUsedAnimation().getHandPos().y+player.getWeapon().getUsedAnimation().getImage().getHeight(null));

			}else{
				y1 = (int) (0)+(int)(player.getPos().y+player.getState().getHandPos().y-player.getWeapon().getStillAnimation().getHandPos().y+player.getWeapon().getStillAnimation().getImage().getHeight(null));

			}
			if(player.getDirection()>0){
				x1 = (int) (player.getPos().x+player.getState().getHandPos().x-player.getWeapon().getStillAnimation().getHandPos().x);
				if(player.weapon.isUsed()){
					ws = player.getWeapon().getUsedAnimation().getOutline();
					

				}else{
					ws = player.getWeapon().getStillAnimation().getOutline();
				}
			}else{
				if(player.weapon.isUsed()){
					x1 = (int) (player.getPos().x+player.getState().getHandPos().x-player.getWeapon().getUsedAnimation().getHandPos().x-player.getWeapon().getUsedAnimation().getImage().getWidth(null));

					ws = player.getWeapon().getMirrorUsedAnimation().getOutline();
				}else{
					x1 = (int) (player.getPos().x+player.getState().getHandPos().x-player.getWeapon().getStillAnimation().getHandPos().x-player.getWeapon().getStillAnimation().getImage().getWidth(null));

					ws = player.getWeapon().getMirrorStillAnimation().getOutline();
				}
			}
			System.out.println(x1+", "+y1);
			for(int a = 0; a<enemies.size();a++){
				Enemy e = enemies.get(a);
				if(player.weapon.isUsed()){
					
				}
				if(Utils.collide(new Polygon2(e.getState().getOutline(), translate(e.getPos(),e.getState().getOutline().getBounds().height)),new Polygon2(ws,translate(new Point2D.Double(x1,y1),0)))){
					e.health-=player.getStats().getDamage()*5;
					//pbullets.remove(i);
					//i--;
					if(e.health<=0){
						enemies.remove(a);
					}
				}
			}
		}
		for(int i = 0;i<ebullets.size();i++){
			Bullet b = ebullets.get(i);
			for(Terrain t:terrain){
				//if(draw.contains(b.design.getOutline().getBounds())&&draw.contains(t.boundingBox.getBounds())){
					if(Utils.collide(new Polygon2(t.boundingBox.poly, translate(t.pos,t.boundingBox.poly.getBounds().height)),new Polygon2(b.design.getOutline(),translate(b.getPos(),b.design.getOutline().getBounds().height)))){
						ebullets.remove(i);
						i--;
					}
				//}
			}
			if(Utils.collide(new Polygon2(player.getState().getOutline(), translate(player.getPos(),player.getState().getOutline().getBounds().height)),new Polygon2(b.design.getOutline(),translate(b.getPos(),b.design.getOutline().getBounds().height)))){
				player.health-=b.damage;
				ebullets.remove(i);
				i--;
				if(player.health<=0){
					lose = true;
				}
			}
			b.translatePos(b.velocity.x, b.velocity.y);
		}
		for(int i = 0;i<pbullets.size();i++){
			Bullet b = pbullets.get(i);
			for(Terrain t:terrain){
				if(Utils.collide(new Polygon2(t.boundingBox.poly, translate(t.pos,t.boundingBox.poly.getBounds().height)),new Polygon2(b.design.getOutline(),translate(b.getPos(),b.design.getOutline().getBounds().height)))){
					pbullets.remove(i);
					i--;
				}
			}
			for(int a = 0; a<enemies.size();a++){
				Enemy e = enemies.get(a);
				if(Utils.collide(new Polygon2(e.getState().getOutline(), translate(e.getPos(),e.getState().getOutline().getBounds().height)),new Polygon2(b.design.getOutline(),translate(b.getPos(),b.design.getOutline().getBounds().height)))){
					e.health-=b.damage;
					pbullets.remove(i);
					i--;
					if(e.health<=0){
						enemies.remove(a);
					}
				}
			}
			b.translatePos(b.velocity.x, b.velocity.y);
		}
		if(enemies.size()<=0){
			win=true;
		}
		for(Terrain t:terrain){
			switch(Utils.cdirection(new Polygon2(player.getState().getOutline(),translate(player.getPos(),player.getState().getOutline().getBounds().height)), new Polygon2(t.boundingBox.poly,translate(t.pos,t.boundingBox.poly.getBounds().height)))){
				case 0:
					//System.out.println("off");
					break;
				case 1:
					//System.out.println("1");
					//e.setJump(true);
					if(player.getVelocity().y>0){
						player.setVelocity(player.getVelocity().x, 0);
					
					}
					break;
				case 2:
					//System.out.println("2");
				
					if(player.getVelocity().x>0){
						player.setVelocity(0, player.getVelocity().y);
						player.setJump(true);
					}
					break;
				case 3:
					//System.out.println("3");
					//e.setJump(true);
					if(player.getVelocity().y<0){
						player.setVelocity(player.getVelocity().x, 0);
						player.setJump(true);
					}
					break;
				case 4:
					//System.out.println("4");
					
					if(player.getVelocity().x<0){
						player.setVelocity(0, player.getVelocity().y);
						player.setJump(true);
					}
					break;
			}
		}
		for(Enemy e:enemies){
			e.setVelocity(e.getVelocity().x, e.getVelocity().y-0.2);
			((Ai)e.controller).update(this);
			e.tick();
			switch(Utils.cdirection(new Polygon2(player.getState().getOutline(),translate(player.getPos(),player.getState().getOutline().getBounds().height)),new Polygon2(e.getState().getOutline(),translate(e.getPos(),e.getState().getOutline().getBounds().height)))){
				case 0:
					//System.out.println("off");
					break;
				case 1:
					//System.out.println("1");
					//e.setJump(true);
					if(player.getVelocity().y>0){
						player.setVelocity(player.getVelocity().x, 0);
					
					}
					break;
				case 2:
					//System.out.println("2");
				
					if(player.getVelocity().x>0){
						player.setVelocity(0, player.getVelocity().y);
						player.setJump(true);
					}
					break;
				case 3:
					//System.out.println("3");
					//e.setJump(true);
					if(player.getVelocity().y<0){
						player.setVelocity(player.getVelocity().x, 0);
						player.setJump(true);
					}
					break;
				case 4:
					//System.out.println("4");
				
					if(player.getVelocity().x<0){
						player.setVelocity(0, player.getVelocity().y);
						player.setJump(true);
					}
					break;
			}
			switch(Utils.cdirection(new Polygon2(e.getState().getOutline(),translate(e.getPos(),e.getState().getOutline().getBounds().height)), new Polygon2(player.getState().getOutline(),translate(player.getPos(),player.getState().getOutline().getBounds().height)))){
				case 0:
					//System.out.println("off");
					break;
				case 1:
					//System.out.println("1");
					//e.setJump(true);
					if(e.getVelocity().y>0){
						e.setVelocity(e.getVelocity().x, 0);
					
					}
					break;
				case 2:
					//System.out.println("2");
				
					if(e.getVelocity().x>0){
						e.setVelocity(0, e.getVelocity().y);
						e.setJump(true);
					}
					break;
				case 3:
					//System.out.println("3");
					//e.setJump(true);
					if(e.getVelocity().y<0){
						e.setVelocity(e.getVelocity().x, 0);
						e.setJump(true);
					}
					break;
				case 4:
					//System.out.println("4");
					
					if(e.getVelocity().x<0){
						e.setVelocity(0, e.getVelocity().y);
						e.setJump(true);
					}
					break;
			}
			//System.out.println(e.getVelocity().y);
			for(Terrain t: terrain){
				//System.out.println(Utils.collide((new Polygon2(e.getState().getOutline(),e.getPos())), (t.boundingBox)));
				switch(Utils.cdirection(new Polygon2(e.getState().getOutline(),translate(e.getPos(),e.getState().getOutline().getBounds().height)), new Polygon2(t.boundingBox.poly,translate(t.pos,t.boundingBox.poly.getBounds().height)))){
					case 0:
						//System.out.println("off");
						break;
					case 1:
						//System.out.println("1");
						//e.setJump(true);
						if(e.getVelocity().y>0){
							e.setVelocity(e.getVelocity().x, 0);
							
						}
						break;
					case 2:
						//System.out.println("2");
						
						if(e.getVelocity().x>0){
							e.setVelocity(0, e.getVelocity().y);
							e.setJump(true);
						}
						break;
					case 3:
						//System.out.println("3");
						//e.setJump(true);
						if(e.getVelocity().y<0){
							e.setVelocity(e.getVelocity().x, 0);
							e.setJump(true);
						}
						break;
					case 4:
						//System.out.println("4");
						
						if(e.getVelocity().x<0){
							e.setVelocity(0, e.getVelocity().y);
							e.setJump(true);
						}
						break;
				}
			}
			e.translatePos(e.getVelocity().x, e.getVelocity().y);
		}
		player.translatePos(player.getVelocity().x, player.getVelocity().y);
		if(win){
			g.setColor(Color.BLUE);
			g.fillRect(0,0,image.getWidth(),image.getHeight());
			g.setColor(Color.WHITE);
			Font f = g.getFont().deriveFont(24f);
			g.setFont(f);
			g.drawString("YOU WIN!", image.getWidth()/2-50, image.getHeight()/2-100);
		}else if(lose){
			g.setColor(Color.BLACK);
			g.fillRect(0,0,image.getWidth(),image.getHeight());
			g.setColor(Color.WHITE);
			Font f = g.getFont().deriveFont(24f);
			g.setFont(f);
			g.drawString("YOU LOSE!", image.getWidth()/2-50, image.getHeight()/2-100);
		}else{
			//g.setColor(Color.BLACK);
			//g.drawPolygon(player.weapon.getUsedAnimation().getOutline());
		g.drawImage(level.getBackground(), null,0, 0);
		for(Bullet b : pbullets){
			g.drawImage(Utils.toBufferedImage(b.design.getImage()), null, (int)b.pos.x-camPos.x, (int)level.getBounds().getHeight()-((int)b.pos.y+b.design.getImage().getHeight(null))-camPos.y);
		}
		for(Bullet b : ebullets){
			g.drawImage(Utils.toBufferedImage(b.design.getImage()), null, (int)b.pos.x-camPos.x, (int)level.getBounds().getHeight()-((int)b.pos.y+b.design.getImage().getHeight(null))-camPos.y);
		}
		int x1;
		int y1;
		if(player.weapon.isUsed()){
			y1 = (int) (level.getBounds().getHeight())-(int)(player.getPos().y+player.getState().getHandPos().y-player.getWeapon().getUsedAnimation().getHandPos().y+player.getWeapon().getUsedAnimation().getImage().getHeight(null));

		}else{
			y1 = (int) (level.getBounds().getHeight())-(int)(player.getPos().y+player.getState().getHandPos().y-player.getWeapon().getStillAnimation().getHandPos().y+player.getWeapon().getStillAnimation().getImage().getHeight(null));

		}
		if(player.getDirection()>0){
			x1 = (int) (player.getPos().x+player.getState().getHandPos().x-player.getWeapon().getStillAnimation().getHandPos().x);
			if(player.weapon.isUsed()){
				g.drawImage(Utils.toBufferedImage(player.getWeapon().getUsedAnimation().getImage()), null, x1-camPos.x, y1-camPos.y);

			}else{
				g.drawImage(Utils.toBufferedImage(player.getWeapon().getStillAnimation().getImage()), null, x1-camPos.x, y1-camPos.y);
			}
		}else{
			if(player.weapon.isUsed()){
				x1 = (int) (player.getPos().x+player.getState().getHandPos().x-player.getWeapon().getUsedAnimation().getHandPos().x-player.getWeapon().getUsedAnimation().getImage().getWidth(null));

				g.drawImage(Utils.toBufferedImage(player.getWeapon().getMirrorUsedAnimation().getImage()), null, x1-camPos.x, y1-camPos.y);
			}else{
				x1 = (int) (player.getPos().x+player.getState().getHandPos().x-player.getWeapon().getStillAnimation().getHandPos().x-player.getWeapon().getStillAnimation().getImage().getWidth(null));

				g.drawImage(Utils.toBufferedImage(player.getWeapon().getMirrorStillAnimation().getImage()), null, x1-camPos.x, y1-camPos.y);
			}
		}

		g.drawImage(Utils.toBufferedImage(player.getState().getImage()),null,(int)player.getPos().x-camPos.x,(int)level.getBounds().getHeight()-((int)player.getPos().y+player.getState().getImage().getHeight(null))-camPos.y);
		for(Enemy e: enemies){
			int x;
			int y = (int) (level.getBounds().getHeight())-(int)(e.getPos().y+e.getState().getHandPos().y-e.getWeapon().getStillAnimation().getHandPos().y+e.getWeapon().getStillAnimation().getImage().getHeight(null));

			if(e.getDirection()>0){
				x = (int) (e.getPos().x+e.getState().getHandPos().x-e.getWeapon().getStillAnimation().getHandPos().x);
				if(e.weapon.isUsed()){
					g.drawImage(Utils.toBufferedImage(e.getWeapon().getUsedAnimation().getImage()), null, x-camPos.x, y-camPos.y);
				}else{
					g.drawImage(Utils.toBufferedImage(e.getWeapon().getStillAnimation().getImage()), null, x-camPos.x, y-camPos.y);
				}
			}else{
				x = (int) (e.getPos().x+e.getState().getHandPos().x-e.getWeapon().getStillAnimation().getHandPos().x-e.getWeapon().getStillAnimation().getImage().getWidth(null));
				if(e.weapon.isUsed()){
					g.drawImage(Utils.toBufferedImage(e.getWeapon().getMirrorUsedAnimation().getImage()), null, x-camPos.x, y-camPos.y);
				}else{
					g.drawImage(Utils.toBufferedImage(e.getWeapon().getMirrorStillAnimation().getImage()), null, x-camPos.x, y-camPos.y);
				}
			}
			g.drawImage(Utils.toBufferedImage(e.getState().getImage()),null,(int)e.getPos().x-camPos.x,(int)level.getBounds().getHeight()-((int)e.getPos().y+e.getState().getImage().getHeight(null))-camPos.y);
		}
		for(Terrain t:terrain){
			if(true){
				g.drawImage(t.image,null,(int)t.pos.x-camPos.x,(int)level.getBounds().getHeight()-((int)t.pos.y+t.image.getHeight())-camPos.y);
			}
			//g.drawPolygon(t.boundingBox.poly);
		}
		}
		
	}
	public Point2D.Double translate(Point2D.Double p, int height){
		return new Point2D.Double(p.getX(), level.getBounds().getHeight()-(p.getY()+height));
	}
	/*public Rectangle translate(Rectangle r){
		return new Rectangle((int)r.getX(), (int)(level.getBounds().getHeight()-(r.getY()+r.getHeight())),(int)r.getWidth(),(int)r.getHeight());
	}
	public Polygon2 translate(Polygon2 p){
		
		Polygon2 p2 = new Polygon2(p.poly, new Point2D.Double(p.pos.getX(), level.getBounds().getHeight()-(p.pos.getY())));
		return p2;
	}*/
	public void addEBullet(Bullet bullet) {
		ebullets.add(bullet);
	}
	public void addPBullet(Bullet bullet) {
		pbullets.add(bullet);
	}
}