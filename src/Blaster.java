import java.awt.Point;
import java.awt.geom.Point2D;

public class Blaster extends Weapon{
	Animation bb;
	//int count = 1000;
	boolean player;
	private boolean used = false;
	public Blaster(boolean player){
		
		setStillAnimation(new Animation(Utils.getImage("res/blaster.png"),new Point(10,10)));
		setUsedAnimation(new Animation(Utils.getImage("res/blaster.png"),new Point(10,10)));
		setMirrorStillAnimation(new Animation(Utils.getImage("res/mblaster.png"),new Point(10,10)));
		setMirrorUsedAnimation(new Animation(Utils.getImage("res/mblaster.png"),new Point(10,10)));
		bb = new Animation(Utils.getImage("res/bb.png"),new Point(0,0));
		this.player = player;
	}
	
	@Override
	public void use(Sprite parent, Model m){
		System.out.println(count);
		if(count>=parent.getStats().getCooldown()){
			if(player){
				System.out.println("p");
				m.addPBullet(new Bullet(bb,new Point2D.Double(parent.getDirection()*12, 0),parent,new Point2D.Double(parent.getPos().x+parent.getState().getHandPos().getX(), parent.getPos().y+parent.getState().getHandPos().getY()),10));
			}else{
				m.addEBullet(new Bullet(bb,new Point2D.Double(parent.getDirection()*12, 0),parent,new Point2D.Double(parent.getPos().x+parent.getState().getHandPos().getX(), parent.getPos().y+parent.getState().getHandPos().getY()),10));
			}
			count = 0;
		}
	}
	
	
}
