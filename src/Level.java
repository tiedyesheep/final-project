import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class Level {
	private ArrayList<Enemy> enemies;
	private ArrayList<Terrain> terrain;
	private Point spawn;
	private Rectangle bounds;
	BufferedImage background;
	public Level(ArrayList<Enemy> enemies, ArrayList<Terrain> terrain, Point spawn, Rectangle bounds, BufferedImage background){
		this.enemies = enemies;
		this.terrain = terrain;
		this.spawn = spawn;
		this.bounds = bounds;
		this.background = background;
	}
	public ArrayList<Enemy> getEnemies() {
		return enemies;
	}
	public ArrayList<Terrain> getTerrain() {
		return terrain;
	}
	public Point getSpawn() {
		return spawn;
	}
	public Rectangle getBounds(){
		return bounds;
	}
	public BufferedImage getBackground(){
		return background;
	}
}
