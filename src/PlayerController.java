import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PlayerController extends Controller implements KeyListener, MouseListener{
	Model m;
	public void setModel(Model m){
		this.m = m;
	}
	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println("key!");
		System.out.println(e.getKeyChar());
		if(e.getKeyChar()==' '){
			jump();
		}else if(e.getKeyChar()=='d'){
			right();
		}else if(e.getKeyChar()=='a'){
			left();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyChar()=='d'){
			stop();
		}else if(e.getKeyChar()=='a'){
			stop();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("click");
		attack(m);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
