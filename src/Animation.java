import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;

public class Animation {

	private Image image;
	private Point handPos;
	private Polygon poly;
	
	public Animation(Image image, Point handPos) {
		this.image = image;
		this.handPos = handPos;
		poly = Utils.getPolygon(image);
	}
	public Image getImage(){
		return image;
	}
	public Point getHandPos(){
		return handPos;
	}
	public Polygon getOutline(){
		return poly;
	}
}
