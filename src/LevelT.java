import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

public class LevelT extends Level{

	public LevelT(ArrayList<Enemy> e, ArrayList<Terrain> t) {
		super(e,t,new Point(500,2000),new Rectangle(0,0,10000,800),null);
	}
	public static LevelT LTFactory(){
		//Terrain t = RectangleTerrain.RTFactory(1000, 100, new Point2D.Double(0, 0), Utils.toBufferedImage(Utils.getImage("res/mgrass.png")));
		ArrayList<Enemy> enemies = new ArrayList<Enemy>();
		for(int i = 0; i<9;i++){
			Enemy e = BasicEnemy.createInstance(new Point2D.Double(500+i*1000,2000));
			enemies.add(e);
		}
		ArrayList<Terrain> terrain = new ArrayList<Terrain>();
		for(int i = 0; i<10;i++){
			Terrain t = RectangleTerrain.RTFactory(1000, 100, new Point2D.Double(i*1000, 0), Utils.toBufferedImage(Utils.getImage("res/mgrass.png")));
			terrain.add(t);
			Terrain t2 = RectangleTerrain.RTFactory(1000, 100, new Point2D.Double(i*1000, 400), Utils.toBufferedImage(Utils.getImage("res/mgrass.png")));
			terrain.add(t2);
		}
		for(int i = 0; i<10;i++){
			Random r = new Random();
			int x = r.nextInt(9000);
			int y = r.nextInt(1000);
			int w = r.nextInt(900)+100;
			int h = r.nextInt(900)+100;
			Terrain t = RectangleTerrain.RTFactory(w, h, new Point2D.Double(x, y), Utils.toBufferedImage(Utils.getImage("res/mgrass.png")));
			terrain.add(t);
		}
		//terrain.add(t);
		return new LevelT(enemies,terrain);
	}
}
