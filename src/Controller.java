
public class Controller {
	Sprite parent;
	public void setParent(Sprite parent){
		this.parent=parent;
		//Thread t = new Thread(new Loop());
	}
	public void forward(){
		parent.forward(parent.getStats().getSpeed()*parent.getDirection());
		if(parent.isOnGround()){
			if(parent.getDirection()==1){
				parent.setState(parent.getBody().getRightAnimation());
			}else{
				parent.setState(parent.getBody().getLeftAnimation());
			}
		}
	}
	public void reverseDirection(){
		parent.setDirection(parent.getDirection()*-1);
	}
	public void jump(){
		if(parent.isOnGround()){
			parent.jump(parent.getStats().getJump());
			parent.setState(parent.getBody().getJumpingAnimation());
		}
	}
	public void attack(Model m){
		parent.useWeapon(m);
		parent.setState(parent.getBody().getStrikingAnimation());
	}
	public void left(){
		parent.setDirection(-1);
		forward();
	}
	public void right(){
		parent.setDirection(1);
		forward();
	}
	public void stop(){
		parent.setVelocity(0, parent.getVelocity().getY());
		if(parent.isOnGround()){
			parent.setState(parent.getBody().getStationaryAnimation());
		}
	}
	/*public void threadFunction(){
		jump();
	}
	public class Loop implements Runnable{

		@Override
		public void run() {
			while(true){
				threadFunction();
			}
		}
		
	}*/
}
