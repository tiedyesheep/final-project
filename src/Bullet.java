import java.awt.geom.Point2D;

public class Bullet {
	Animation design;
	Point2D.Double velocity;
	Sprite source;
	Point2D.Double pos;
	double damage;
	public Bullet(Animation design, Point2D.Double velocity, Sprite source, Point2D.Double pos, double damage) {
		this.design = design;
		this.velocity = velocity;
		this.source = source;
		this.pos = pos;
		this.damage = damage;
	}
	public void setPos(Point2D.Double pos){
		this.pos = pos;
	}
	public Point2D.Double getPos(){
		return pos;
	}
	public Point2D.Double getVelocity(){
		return velocity;
	}
	public void translatePos(double x, double y){
		pos = new Point2D.Double(pos.getX()+x, pos.getY()+y);
		
	}
}
