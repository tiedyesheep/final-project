
public class Stats {
	private int speed;
	private int damage;
	private int jump;
	private int cooldown;
	private double health;
	public Stats(int speed, int damage, int jump, int cooldown, double health) {
		this.speed = speed;
		this.damage = damage;
		this.jump = jump;
		this.cooldown = cooldown;
		this.health = health;
	}
	public int getSpeed() {
		return speed;
	}
	public int getDamage() {
		return damage;
	}
	public int getJump() {
		return jump;
	}
	public int getCooldown() {
		return cooldown;
	}
	public double getHealth(){
		return health;
	}
}
