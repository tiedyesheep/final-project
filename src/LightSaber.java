import java.awt.Point;
import java.awt.geom.Point2D;

public class LightSaber extends Weapon {
	public LightSaber(){
		setStillAnimation(new Animation(Utils.getImage("res/rsaberstill.png"),new Point(10,10)));
		setUsedAnimation(new Animation(Utils.getImage("res/rsaberused.png"),new Point(10,10)));
		setMirrorStillAnimation(new Animation(Utils.getImage("res/rsaberstill.png"),new Point(10,10)));
		setMirrorUsedAnimation(new Animation(Utils.getImage("res/rmsaberused.png"),new Point(10,10)));
		dangerous = true;
	}
	@Override
	public void tick(){
		count++;
		if(isUsed()){
			if(count>=5){
				setUsed(false);
				count = 0;
			}
		}
	}
	@Override
	public void use(Sprite parent, Model m){
		if(!isUsed()){
			if(count>=parent.getStats().getCooldown()){
				setUsed(true);
				count = 0;
			}
		}
	}
	
}
