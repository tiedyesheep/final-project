import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

public class RectangleTerrain extends Terrain{

	public RectangleTerrain(Polygon2 boundingBox,Color c, Point2D.Double pos) {
		super(boundingBox, c, pos);
	}
	public RectangleTerrain(Polygon2 boundingBox,BufferedImage c, Point2D.Double pos) {
		super(boundingBox, c, pos);
	}
	public static RectangleTerrain RTFactory(int width, int height, Point2D.Double pos, Color c){
		int[] xpoints = new int[4];
		int[] ypoints = new int[4];
		xpoints[0]=0;
		ypoints[0]=0;
		xpoints[1]=width;
		ypoints[1]=0;
		xpoints[2]=width;
		ypoints[2]=height;
		xpoints[3]=0;
		ypoints[3]=height;
		Polygon poly = new Polygon(xpoints,ypoints,4);
		Polygon2 p2 = new Polygon2(poly,new Point2D.Double(0,0));
		return new RectangleTerrain(p2,c,pos);
	}
	public static RectangleTerrain RTFactory(int width, int height, Point2D.Double pos, BufferedImage c){
		int[] xpoints = new int[4];
		int[] ypoints = new int[4];
		xpoints[0]=0;
		ypoints[0]=0;
		xpoints[1]=width;
		ypoints[1]=0;
		xpoints[2]=width;
		ypoints[2]=height;
		xpoints[3]=0;
		ypoints[3]=height;
		Polygon poly = new Polygon(xpoints,ypoints,4);
		Polygon2 p2 = new Polygon2(poly,new Point2D.Double(0,0));
		return new RectangleTerrain(p2,c,pos);
	}

}
