import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

public class Polygon2 {
	Polygon poly;
	Point2D.Double pos;
	public Polygon2(Polygon poly, Point2D.Double pos) {
		this.poly = poly;
		this.pos = pos;
	}
	public boolean contains(int x, int y){
		return(poly.contains(x-pos.getX(), y-pos.getY()));
	}
	public Rectangle getBounds() {
		return poly.getBounds();
	}
	public int npoints(){
		return poly.npoints;
	}
	public int[] xpoints(){
		return poly.xpoints;
	}
	public int[] ypoints(){
		return poly.ypoints;
	}
	public Point2D.Double getPos(){
		return pos;
	}
}
