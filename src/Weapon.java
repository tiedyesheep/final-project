import java.awt.Image;

public class Weapon {
	boolean used = false;
	boolean dangerous = false;
	int count = 1000;
	private Animation[] animations;
	public Weapon() {
		this.animations = new Animation[4];
	}
	public void setStillAnimation(Animation a){
		animations[0] = a;
	}
	public void setUsedAnimation(Animation a){
		animations[1] = a;
	}
	public void setMirrorStillAnimation(Animation a){
		animations[2] = a;
	}
	public void setMirrorUsedAnimation(Animation a){
		animations[3] = a;
	}
	public Animation getStillAnimation(){
		return animations[0];
	}
	public Animation getUsedAnimation(){
		return animations[1];
	}
	public void use(Sprite parent, Model m){
		//nothing
	}
	public Animation getMirrorStillAnimation() {
		return animations[2];
	}
	public Animation getMirrorUsedAnimation() {
		return animations[3];
	}
	public void setUsed(boolean u){
		used = u;
	}
	public boolean isUsed(){
		//used = !used;
		return used;
	}
	public void tick(){
		count++;
	}
	
}
