import javax.swing.JFrame;

public class TestFrame extends JFrame{

	public TestFrame() {
		this.setSize(1000, 800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Test t = new Test();
		this.addKeyListener((PlayerController)t.m.player.controller);
		this.addMouseListener((PlayerController)t.m.player.controller);
		this.add(t);
		this.setVisible(true);
	}
	public static void main(String[] args){
		new TestFrame();
	}
}
