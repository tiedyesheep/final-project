import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class CustomGraphics{
	Graphics2D g;
	Rectangle bounds;
	public CustomGraphics(Graphics2D g, BufferedImage img){
		this.g = g;
		bounds = new Rectangle(0,0,img.getWidth(),img.getHeight());
	}

}
